function [nbBits,peaksnr] = codec(varargin)
%CODEC Summary of this function goes here
%   Detailed explanation goes here

HEVCDir = HEVC.Dir;
defaultCfg  = fullfile(HEVCDir,'cfg','encoder_intra_main_rext.cfg');

%% Parse coding choice
codecp = inputParser; codecp.KeepUnmatched = true; codecp.StructExpand = true;
codecp.addParameter('encode', true, @islogical);
codecp.addParameter('decode', true, @islogical);

codecp.parse(varargin{:});
encode = codecp.Results.encode;
decode = codecp.Results.decode;

%% Select executable according to coding choice and OS
if encode
    if ismac
        error('Platform not yet supported')
    elseif isunix
        TApp = fullfile(HEVCDir,'bin','TAppEncoderStatic');
    elseif ispc
        TApp = fullfile(HEVCDir,'bin','TAppEncoder.exe');
    end
elseif decode
    if ismac
        error('Platform not yet supported')
    elseif isunix
        TApp = fullfile(HEVCDir,'bin','TAppDecoderStatic');
    elseif ispc
        TApp = fullfile(HEVCDir,'bin','TAppDecoder.exe');
    end
else
    warning('Nothing to do');
    [nbBits,peaksnr] = deal(NaN);
    return;
end

%% Select HEVC parameters to match according to coding choice
HEVCp = inputParser; HEVCp.KeepUnmatched = true; HEVCp.StructExpand = true;
HEVCp.addParameter('WarnUnknowParameter', '0', @ischar);

if encode
    HEVCp.addParameter('ConfigFile'       , defaultCfg, @ischar);
    HEVCp.addParameter('InputFile'        , 'ref.yuv' , @ischar);
    HEVCp.addParameter('BitstreamFile'    , 'bit.hevc', @ischar);
    HEVCp.addParameter('FrameRate'        , '30'      , @ischar);
    HEVCp.addParameter('FramesToBeEncoded', '0'       , @ischar);
    if decode
        HEVCp.addParameter('ReconFile', 'rec.yuv' , @ischar);
    end
elseif decode
    HEVCp.addParameter('BitstreamFile', 'bit.hevc', @ischar);
    HEVCp.addParameter('ReconFile'    , 'rec.yuv' , @ischar);
end

%% Parse HEVC arguments
HEVCp.parse(codecp.Unmatched);

HEVCParams = HEVCp.Results;
extraHEVCParams = HEVCp.Unmatched;

% Make sure to create output folders before running executable
[folder,~,~] = fileparts(HEVCParams.BitstreamFile);
if ~exist(folder,'dir')
    mkdir(folder);
end
if decode
    [folder,~,~] = fileparts(HEVCParams.ReconFile);
    if ~exist(folder,'dir')
        mkdir(folder);
    end
end

[LogFolder,LogName,~] = fileparts(HEVCParams.BitstreamFile);
LogFileEnc = fullfile(LogFolder,[LogName,'_enc.rtf']);
LogFileDec = fullfile(LogFolder,[LogName,'_dec.rtf']);

%% Execute command
HEVCArgList      = paramToArgList(HEVCParams);
extraHEVCArgList = paramToArgList(extraHEVCParams);

% Prompt in terminal and write in log file
if ispc
    redirect = @(logfile) ['> "',logfile,'" | type "' logfile, '"'];
elseif isunix
    redirect = @(logfile) ['| tee "' ,logfile, '"'];
end

if encode
    redirectLog = redirect(LogFileEnc);
elseif decode
    redirectLog = redirect(LogFileDec);
end

command = ['"' TApp,'" ',HEVCArgList,' ',extraHEVCArgList,' ',redirectLog];

% Execute command
disp(command)
status = system(command);

if status
    error('HEVC Error');
end

% Read compression statistics from log file
try
    [nbBits,peaksnr] = HEVC.parseLog(LogFileEnc);
catch
    [nbBits,peaksnr] = deal(NaN);
end

end

function argList = paramToArgList(Params)

% Get parameters and arguments
parameters = fieldnames (Params);
values     = struct2cell(Params);

% Deal with '-c' being the single command line parameter for configuration file 
indcfg = strcmp(parameters,'ConfigFile');

if any(indcfg)
    argcfg = ['-c "',values{indcfg},'"'];
else
    argcfg = '';
end

parameters = parameters(~indcfg);
values     = values    (~indcfg);

% Build command line argument list
argList = cellfun(@(param,val) ['--',param,'="',val,'"'],parameters,values,'UniformOutput',false);

argList = strjoin([argcfg;argList(:)]);
end